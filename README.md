---
marp: true
author: ILLOUZ Maël
title: Formation Git & Overview GitLab
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('./img/hero-background.svg')
style: |
  section {
    font-size:1.6em;
  }

---
![bg left:40% 90%](./img/git.png)
![bg left:40% 130%](./img/gitlab.png)

# **Git & GitLab**

Formation Git & Overview GitLab

---
# Git en quelques mots

Git est un système de contrôle de version open-source

## Caractéristiques
  - Versionner les fichiers (retour arrière)
  - Dépôt en local et/ou dépôts distants GitLab/Github
  - Compatible windows, linux, mac, solaris
  - Opérations atomiques
  - Répertoire `.git` contient toutes les informations
<!--
Commentaires:
- Versionner des fichiers
  - code source de vos applications
  - fichiers de configuration pour vos serveurs
  - code source de votre Infra As Code (IaC)
  - documentations
  - tous types de fichiers
- atomique : Cela signifie qu'une action peut soit réussir soit échouer (sans aucune altération)
-->
---
# GitLab en quelques mots

GitLab est une plate-forme de développement logiciel open source de bout en bout avec 
- contrôle de version intégré
- suivi des problèmes
- révision du code
- CI/CD
- Wiki
- etc...

GitLab accueil des projets `git` et permet de centraliser les développements sur un espace commun.

<!--
Commentaires:
On travail en local son code et on push les éléments sur GitLab
-->
---

# GitLab - Présentation Live

![bg 50% ](./img/gitlab.png)

---

![bg left:40% 90%](./img/git.png)

# **Sommaire Git**

- Installation
- Configuration
- Opérations de base
  - cloner un projet
  - commit / pull / push
  - tag / branch / merge
  - résoudre un conflit
- Tracer les changements (log/diff/reset)
- Résumé des opérations
- Atelier
---

# Git - Installation

### Windows
  - [git-scm](https://git-scm.com/download/win) - installation en administrateur
### Linux
```bash
sudo apt-get install git
```
### Verifier la version installée
```
git --version
```
si la commande git n'est pas reconnue, vérifier votre variable d'environnement `PATH`

---
# Git - Configuration

 visualiser vos paramétres et leurs provenances
```bash
git config --list --show-origin
```
configurer votre identité (en relation avec votre identité GitLab)
```bash
git config --global user.name "John Doe"
git config --global user.email johndoe@example.com
git config --global credential.helper cache # password cache pendant 15min
git config --global credential.helper store # stocker identifiant
```
quelques paramétrages complémentaires
```bash
git config --global init.defaultBranch main # définir le nom de la branch par defaut
git config --global http."https://code.example.com/".sslCAInfo <downloaded certificate>.pem # déclarer un certificat autosigné
# ou
git config --global http.sslVerify false # désactiver la vérification du certificat SSL
```

---
# Git - opérations de base

### cloner un projet

```bash
git clone <repository url> # gitlab / github ...
cd <repo dir>
```

### préparer votre futur commit (staging)

créer, modifier ou supprimer un ou des fichiers.
```bash
git status    # faire un état des lieux des fichiers
git add .     # <file> ajouter les modifications à votre futur commit
```
<!--
Commentaires:
afin de ne pas prendre en compte certains fichiers, on peut ajouter un fichier .gitignore dans le répertoire
-->

### commit des modifications sur votre repository local
```bash
git commit -m "mon 1er commit" # commiter avec un message (obligatoire)
git commit -a                  # git add . + commit + lance éditeur pour saisir un message de commit
```
---
### Récupérer les éventuels changements de la branche distante

```bash
# distant => local
git pull    # effectue un merge automatique si necessaire 
```

### Pousser les nouveaux changements sur la branche distante

```bash
# local => distant
git push    # maintenant envoyer vos changements sur le site distant
```
### Taguer la version

permet de livrer une version du code pour continuer 
```bash
git tag -l      # lister les tags existant
git tag 0.1.2   # fixer une version du code
git tag 0.1.2 -m "messsage" # ajouter un texte pour accompagner le tag
git push --tag  # pousser le tag sur le site distant
```
---
### Les branches
Elles permettent de faire vivre séparément le code. (création d'un patch, d'une évolution...)

```bash
git checkout -b patch-1 # creation et switch sur la branch
git branch              # lister les branches (* = active)
git checkout <branch>   # switch de branche
git branch -d <branch>  # suppression de la branche (non active)
```
Lorsque le travail est terminée sur la branche, il faut fusionner `merge` avec la branch main
```bash
git checkout main       # switch sur la branch principal
git merge patch-1       #

git merge <branch>   # essaiera de fusionner la branche donnée dans la branche actuelle
git merge --abort    # en cas de conflits de fusion, vous pouvez abandonner la fusion
git merge --continue # continuer après la résolution du conflit
```

---
### Conflit à résoudre

Utiliser simplement VSCode
- ouvrir le fichier en conflit 
- accepter ou non les changements (outils de comparaison inclus)
- sauvegarder les changements et refaite un `git add` + `git commit`

ou un outil pour comparer 
```bash
git mergetool
git mergetool --tool-help # liste tous les outils compatibles
# sous Windows avec l'outil Meld par exemple
git config --global merge.tool "meld"
git config --global mergetool.meld.path "C:\Program Files (x86)\Meld\Meld.exe"
```

---
# Git - tracer les changements

Il est important de pouvoir tracer les versions et obtenir une visualisation de celles-ci

```bash
git log
git log --oneline
git log --oneline --graph
git log --oneline --graph --name-status
git log --oneline --graph --name-status <file>
```
Personalisé :
```bash
git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr)%Cblue - %cn %Creset' --abbrev-commit --date=relative
```

---
# Tig - explorer un projet Git

Permet d'obtenir un ensemble d'informations en parcourant l'arborescence présentée par l'interface. Vous pourrez, par exemple, consulter l'historique d'un fichier, voir les différences entre les différents commits, réaliser un git blame et bien d'autres actions. Ces différentes opérations sont caractérisées par une vue dans Tig.

![bg right:60% 99%](./img/tig.png)

---

### Différence entre 2 versions
```bash
git diff <commit sha1> <commit sha2>
git diff HEAD^^ HEAD           # difference entre commit courant(HEAD) et 2 commits précedent HEAD^^ 
git diff HEAD^^ HEAD -- <file> # sur un fichier
git diff <branche-source> <branche-cible> # entre 2 branches
```
### Revenir en arrière

```bash
git reset                         # désindex pour revenir au dernier commit
git reset <file>                  # désindex pour revenir au dernier commit
git reset <commit sha>            # revenir au commit
git reset <commit sha> -- <file>  # revenir au commit pour un fichier
git reset --hard HEAD             # réinitialiser l'index et le répertoire de travail à l'état du dernier commit
git reset --hard <commit sha>     # idem répertoire de travail rééinit
```

---
# Git - Résumé des opérations

![bg 57%](./img/git-transport.png)

<!--
Commentaires:
- Workspace : votre répertoire local
- Staging : état avant commit (liste les éléments qui constituera le commit)  - Index
- Local repository : votre dépot local (HEAD) 
- Remote repositoy : le dépot distant GitLab
-->

---
# Atelier GitLab / Git

- créer votre projet sur GitLab (avec un fichier Readme.md)
- créer un token Gitlab
- configurer votre logiciel git local
- cloner le repository
- ajouter un fichier texte + modifier le Readme.md
- git status + add + commit + push (constater sur GitLab)
- recommencer en supprimant le fichier créé et en remodifiant le Readme.md
- git status + add + commit + push (constater sur GitLab)
- créer une branche
- ajouter un fichier + add/commit/push (constater sur GitLab)
- créer une merge Request sur GitLab
- valider la merge request et constater sur le changement sur la branche `main`

---
# Annexe - Complément pour initialiser un projet

plusieurs choix pour initialiser un projet sur votre PC

### depuis un répertoire local contenant votre projet

```bash
cd <monprojet>
git init                                    # initaliser le repertoire en tant que projet git
git remote add origin https://gitlab.com/monespace/git.git # relier votre projet à un projet GitLab
git checkout -b main                        # créer une branch main
git add .                                   # ajouter tous les fichiers en attente
git commit -m "init projet"                 # valider la version
git push --set-upstream origin main         # pousser la branch vers le site distant
```
### depuis un dépot distant existant

```bash
git clone https://gitlab.com/monespace/git.git
```
téléchargement du dépot sur votre PC avec toute la configuration
